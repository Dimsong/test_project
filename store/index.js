import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
import city from './modules/city.js'
import category from './modules/category.js'
import news from './modules/news.js'

const vuexLocal = new VuexPersistence({
  modules: ['city', 'category']
})

Vue.use(Vuex)

const main_store = () => new Vuex.Store({
  modules: {
    city, category, news
  },
  plugins: [vuexLocal.plugin]
})

export default main_store
