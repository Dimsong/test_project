import category_data from '~/data/category.js'

const DEFAULT_STATE = () => {
  return {
    list: category_data.map((item) => {
      item.checked = false,
      item.count = 0
      return item
    })
  }
}

export default {
  state: DEFAULT_STATE(),
  namespaced: true,
  mutations: {
    updateCategory(state, message) {
      state.list.map((item) => {
        if (item.id == message.id) {
          item.checked = message.checked
        }
        return item
      });
    }
  }
}
