import city_data from '~/data/city.js'

city_data.unshift({
  id: -1,
  name: 'All cities'
})

const DEFAULT_STATE = () => {
  return {
    list: city_data,
    selected: city_data[0].id
  }
}

export default {
  state: DEFAULT_STATE(),
  namespaced: true,
  mutations: {
    updateCity(state, message) {
      const selectedItem = state.list.find(item => item.id == message)
      state.selected = selectedItem
    }
  }
}
